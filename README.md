# Exploration of whether different lifestages of groundfish have different distributions

__Main author:__  Sarah Davies, Ashley Park, Beatrice Proudfoot, Patrick Thompson 
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Pacific Biological Station  
__Contact:__      e-mail: Sarah.Davies@dfo-mpo.gc.ca | tel:


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
  + [Subsections within contents](#subsections-within-contents)
- [Methods](#methods)
  + [Subsections within methods](#subsections-within-methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Explore different lifestages of groundfish species will respond to changes in temperature and oxygen due to climate change.


## Summary
Combine NOAA and DFO survey data (both presence/absence and lengths) to look at how different lifestages of species respond to temperature, oxygen and depth across large latitudinal gradient.


## Status
In-development


## Contents
Describe the contents of the repository. Are there multiple scripts or directories? What are there purpose and how do they relate to each other?
### Subsections within contents
Use subsections to describe the purpose of each script if warranted.


## Methods
What methods were used to achieve the purpose? This should be detailed as possible.
### Subsections within methods
Often useful to organise the methods under multiple subheadings.


## Requirements
*Optional section.* List the input data requirements or software requirements to successfully execute the code.


## Caveats
Anything other users should be aware of including gaps in the input or output data and warnings about appropriate use.


## Uncertainty
*Optional section.* Is there some uncertainty associated with the output? Assumptions that were made?


## Acknowledgements
*Optional section.* List any contributors and acknowledge relevant people or institutions


## References
*Optional section.*
